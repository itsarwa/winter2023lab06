import java.util.Random;
public class Die{
  private int faceValue;
  private Random randnum;

  public Die(){
    this.faceValue=1;
    this.randnum=new Random();
    
  }
  public int getFaceValue(){
    return this.faceValue;
  }
  public void roll(){
    this.faceValue = randnum.nextInt(6);

  }
  public String toString(){
    return Integer.toString(faceValue);
  }
}