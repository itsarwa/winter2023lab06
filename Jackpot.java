public class Jackpot{
  public static void main(String[] args){
    System.out.println("Welcome to Jackpot!");
    Board board = new Board();
    boolean gameOver = false;
    int numOfTilesClosed = 0;

    while(!gameOver){
      System.out.println(board);
      if(board.playAturn()){
        gameOver=true;
        
      }else{
        numOfTilesClosed++;
      }
    }
    if(numOfTilesClosed>=7){
      System.out.println("You have reached Jackpot! You won! ");
    }else{
      System.out.println("Oh no! You lost! Try again");
    }
    }
  }
