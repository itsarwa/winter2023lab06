public class Board{
  private Die die1;
  private Die die2;
  private boolean[] tiles;

  public Board(){
    this.die1= new Die();
    this.die2= new Die();
    this.tiles= new boolean [12];
  }
  public String toString(){
    String tilesPos= " ";

    for (int i=0; i < this.tiles.length; i++){
      if(this.tiles[i]==false){
      tilesPos+=i+1+" ";
    }else{
      tilesPos= " X ";
      
    }
  }
  return tilesPos;
  
}
  public boolean playAturn(){
    this.die1.roll();
    this.die2.roll();

    System.out.println("Die 1: " +die1.toString());
    System.out.println("Die 2: " +die2.toString());

  int sumOfDice=die1.getFaceValue() + die2.getFaceValue();

    if(this.tiles[sumOfDice-1]==false){
      this.tiles[sumOfDice-1]=true;
      System.out.println("Closing tile equal to sum: " +sumOfDice);
      return false;
      
    }else if(this.tiles[die1.getFaceValue()-1]==false){
      this.tiles[die1.getFaceValue()-1]=true;
      System.out.println("Closing tile with the same value as die one "+die1.getFaceValue());
      return false;
      
      }else if(this.tiles[die2.getFaceValue()-1]==false){
      this.tiles[die2.getFaceValue()-1]=true;
      System.out.println("Closing tile with the same value as die two "+die2.getFaceValue());
      return false;
      
    }else{
      System.out.println("All the tiles for these values are already shut");
      return true;
    }
  }
    }
  



